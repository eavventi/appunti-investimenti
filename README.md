Con questo document voglio motivare e proporre dei cambiamenti di strategia per gli investimenti di famiglia.
L'obbiettivo é quello di proteggere il patrimonio da quelli che a mio avviso saranno i rischi principali del prossimo decennio.
Perdonatemi gli accenti sbagliati che non riesco proprio a digitarli giusti con la tastiera svedese.

# Inflazione

Gli ultimi venti anni sono stati caratterizzati da una concomitanza eccezionale di forze deflazionarie.
Il primo evento, in ordine cronologico, é stato l'entrata della Cina nel mercato mondiale.
A seguito di un industrializzazione a ritmi vertiginosi, la popolazione Cinese ha potuto contribuire
ad una espansione della forza lavorativa mondiale con pochi precedenti nella storia.
Una tale espansione é altamente deflazionaria perché il lavoratore medio produce piú di quanto consuma,
ancor piú nel caso del lavoratore Cinese.

Il secondo evento é una diretta conseguenza dell'industrializzazione Cinese che sembrava non avere limiti.
Per favorire la globalizzazione e saziare il sempre crescente appetito Cinese per le materie prime,
il resto del mondo ha effettuato ingenti investimenti in infrastrutture, come per esempio il trasporto
marittimo, e attivitá estrattive di materie prime come il ferro o il rame.
Con la crisi finanziaria del 2008, l'espansione dell'economia Cinese ha subito un brusco rallentamento
e gli investimenti effettuati sulla base della crescita Cinese pre-crisi hanno portato ad un crollo
nei rispettivi settori.
Grazie al naturale ritardo tra il finanziamento e il rispettivo aumento di offerta, i prezzi sono rimasti a
livelli bassi per piú di un decennio. 

Le uniche materie prime i cui prezzi si sono ripresi dopo la crisi sono stati i combustibili fossili.
Infatti, allora la produzione di petrolio era stagnante e si parlava spesso del picco della produzione e di una
possibile crisi energetica.
Per fortuna un'ulteriore forza deflazionaria era giá in movimento.
A partire dal 2005, nuove tecniche di estrazione del petrolio sono state sviluppate negli USA:
la fratturazione idraulica (fracking) e la trivellazione orizzontale.
La combinazione delle due ha portato alla nascita dell'industria Americana del petrolio Shale.
Grazie a questa rivoluzione, gli USA sono diventati in pochi anni i primi produttori mondiali di petrolio.
Il cambiamento é stato ancora piú notevole nell'ambito del gas naturale dove da nazione importatore,
l'America é diventata uno dei maggiori esportatori mondiali.
Come spesso accade durante i boom economici, ingenti somme sono state investite in quest'industria
attratte da promesse di guadagni straordinari.
La festa é finita nel 2016 quando OPEC, che di solito é dedito a mantenere i prezzi del petrolio stabili,
si é stufato di perdere quote di mercato e ha inondato il mercato facendo crollare i prezzi e mandando
gran parte dell'industria Shale in bancarotta.
Le conseguenti perdite ingenti non sono peró state incorse in vano dagli investitori ma hanno contribuito ad anni
di abbondanza energetica per gli USA in particolare ma anche per il mondo.


## Il ritorno dell'inflazione

L'inflazione é tornata di prepotenza a partire dal 2021 a seguito di un programma di stimoli eccezionale,
specialmente quello Americano, a risposta della crisi del Covid.
L'invasione dell'Ucraina da parte della Russia ha contribuito ad aumentare l'impulso inflazionario,
particolarmente per l'Europa.
Guardando al futuro, é ragionevole pensare che l'inflazione scenderá dai livelli molto elevati odierni.
Infatti, il costo elevato dell'energia e/o gli aumenti delle rate coordinati tra le diverse banche centrali
freneranno la crescita economica, potenzialmente mandando l'economia mondiale in recessione.

Detto questo, rimane la questione se l'inflazione tornerá ai livelli bassi che hanno caratterizzato gli ultimi anni.
Secondo il mio parere, lo scenario piú probabile per il prossimo decennio sará caratterizzato da un inflazione
che oscillará attorno ad un livello medio piuttosto elevato, diciamo il 4-5%.
Tali oscillazioni, peró, potrebbero essere molto violente.
Per esempio, penso che ci sia un rischio significativo di una devalutazione improvvisa dello Renminbi/Yuan.
Un tale evento porterebbe l'economia globale ad un dei piú grandi shock deflazionari della storia
ma sarebbe comunque un evento isolato nel tempo.

Guardando al lungo periodo, le dinamiche di cui ho parlato in precedenza sembrano aver esaurito il proprio effetto deflazionario
e potrebbero addirittura contribuire in senso opposto.
Infatti, la popolazione Cinese in etá lavorativa é destinata a ridursi significativamente nell'arco del prossimo ventennio.
L'unica possibilitá di sfuggire a questo destino, l'immigrazione, sembra essere impossibile da praticare.
Oltre la Cina ci sono altri bacini potenziali per incrementare la forza lavorativa mondiale come l'India e l'Africa
ma la disposizione dell'occidente verso la globalizzazione é cambiata notevolmente dal 2000 e,
a seguire della crisi finanziaria del 2008, le possibilitá di finanziamento sono diminuite.
Inoltre, l'India e tantomeno l'Africa non possono avere lo stesso livello di pianificazione di una nazione autoritaria come la Cina.

Per quanto riguarda i mercati delle materie prime, la situazione é varia ma in molti casi la scarsitá di
investimenti del decennio passato ha giá rallentato la crescita dell'offerta.
Questo vale ancor di piú nel settore energetico ma questo argomento merita una sezione propria.

In aggiunta, anche le dinamiche demografiche dell'occidente rischiano di contribuire all'impulso inflazionario.
Il prossimo decennio vedrá la maggior parte dei baby boomers lasciare la forza lavorativa e andare in pensione.
Allo stesso tempo i millennial saranno nel periodo di formazione familiare, dove le coppie comprano e
arredano la propria casa. Questo viene fatto generalmente a credito, anticipando cioé il consumo potenziale futuro.
Entrambe queste dinamiche sono inflazionarie.


# Energia

La situazione riguardo l'energia é ovviamente molto complicata, colta al centro di un conflitto politico/scientifico riguardo il cambiamento climatico.
Quest'ultimo é un argomento molto sentito e polarizzante e, in questi casi, bisogna essere pragmatici e guardare ai fatti.

## Combustibili fossili

Partiamo dal fatto che il mondo é dipendende dai combustibili fossili. Nel 2019, 81% del consumo energetico globale
é stato soddisfatto da combustibili fossili.
Il consumo di combustibili fossili generalmente aumenta ogni anno in termini assoluti,
le uniche eccezioni nel 2009 e 2020 se si guarda solo a petrolio e gas naturale.
L'uso del carbone era in leggero declino negli ultimi anni ma si é ripreso nel 2021 e
il 2022 rappresenterá molto probabilmente un nuovo consumo record.

![Fonti energetiche globali](images/total-energy-by-source.png)

La produzione di petrolio e gas naturale, che rappresentano piú della metá della produzione energetica,
diminuisce naturalmente di circa il 6-7% ogni anno in mancanza di nuovi investimenti.
Nonostante questo, le aziende del settore non sono piú propense ad investire.
Dal 2020, gli investimenti sono rimasti sempre inferiori rispetto ai livelli del 2016, un anno di crisi nel settore.

![Investimenti in combustibili fossili](images/fossil_fuels_investments.jpeg)

Le ragioni per questo sotto-investimento sono molteplici.
Da una parte, gli investitori stessi hanno visto enormi quantitá di capitale bruciate durante l'ultima espansione del settore
e ora insistono per un ritorno del capitale, per esempio sotto forma di dividendi.
Oltre tutto, se si crede nella transizione energetica e quindi ad un'eventuale diminuzione della domanda,
é del tutto razionale diminuire gli investimenti.
Dall'altra c'é la pressione da parte del movimento ecologista volta a scoraggiare nuovi progetti.
Infatti, sembra essere un obbiettivo preciso del movimento quello di rendere i combustibili fossili piú cari
in modo da facilitare la transizione.

Con queste premesse, sembra inevitabile che il settore dei combustibili fossili continuerá a contribuire
alle pressioni inflazionarie per i prossimi anni. 
L'unica soluzione nel medio periodo é un'aumento significativo degli investimenti
di cui non si vedono tracce per il momento.

Con queste premesse é spontaneo domandarsi se é opportuno allocare una porzione significativa del portfolio al settore.
La risposta non é scontata perché, sebbene le azioni di aziende petrolifere seguono i prezzi del combustibili fossili
nel breve periodo, la fonte di guadagni nel lungo periodo é legata ad altre metriche.
Specificamente, il ritorno sul capitale impiegato (ROCE) é di vitale importanza.
Nel grafico qui sotto riporto il ROCE assieme al peso del settore (XLE) all'interno dell'indice S&P 500 negli ultimi tre decenni.

![Energia ROCE](images/energy-ROE-VS-SPY-weighting.jpeg)

Un livello alto di ROCE sostenuto negli anni risulta in ritorni positivi rispetto al mercato.
Cosa influenza il ROCE?
Ovviamente i prezzi del greggio e gas naturale sono molto importanti e ho giá presentato la mia tesi che prevede
un livello medio elevato nei prossimi anni.
Dall'altra parte, i costi dei servizi accessori come trivellazione, manutenzione e trasporto sono giá aumentati nel 2022
a dispetto del livello basso di investimenti e continueranno ad aumentare nei prossimi anni.
Gli interessi sul debito é un altro fattore che potrebbe contribuire negativamente al ROCE con il recente aumento generale delle rate.
Per fortuna la maggioranza delle aziende del settore dovrebbero arrivare ad ottenere un livello di debito netto (debito meno liquiditá) molto basso alla fine di quest'anno,
addirittura negativo per alcune.
Per finire c'é il rischio piú grande: le tasse.
I super profitti delle aziende petrolifere fanno gola a politici di tutto il mondo in un periodo durante il quale la persona media sente pungere l'inflazione.
Nonostante questi rischi, penso sia auspicabile un investimento significativo nel settore.

## Fonti rinnovabili

Le fonti energetiche cosiddette rinnovabili, principalmente fotovoltaico ed eolico, sono state scelte come
la principale soluzione per la decarbonizzazione del settore energetico.
In seguito alla guerra in Ucraina, molti governi occidentali hanno segnalato l'intenzione di raddoppiare gli sforzi in questo senso.
Personalmente penso che questa strategia sia completamente irrazionale e dettata piú dall'ideologia che da considerazioni tecniche.
Ho paura che il risultato finale sará uno spreco senza precedenti di risorse preziose ed una conseguente aumento del costo dell'energia.
Le ragioni dietro questa previsione sono molteplici.

Per prima cosa bisogna osservare che l'acclamato sviluppo tecnologico/manufatturiero che ha abbattuto il costo per kW delle rinnovabili
é coinciso con un periodo di prezzi particolarmente bassi sia per le materie prime che per i combustibili fossili.
Non a caso gli studi piú citati circa questo ambito analizzano il decennio precedente al 2020.
Durante il 2021 il prezzo del silicio policristallino, componente chiave per i pannelli fotovoltaici, é letteralmente esploso.

![Silicio policristallino](images/polysilicon-price.jpeg)

L'ascesa non si é fermata e attualmente (17 Nov, 2022) si trova attorno ai 37$/kg.
Cos'é successo? La produzione di silicio policristallino é molto intensiva energeticamente e il prezzo é salito assieme al costo
dell'energia in Cina.
L'ironia é che la generazione elettrica Cinese deriva in gran parte dal carbone.
Per inciso, prendere in considerazione i dieci anni fino al 2019 per mostrare l'evoluzione dei prezzi dei pannelli fotovoltaici é doppiamente fuorviante
perché nel 2006-2009 il mercato del silicio policristallino si trovava in uno stato di estremo disequilibrio con prezzi che hanno raggiunto 475$/kg.
Anche il settore eolico ha visto i propri costi esplodere.
Per esempio, Vestas wind systems AS che é uno dei principali produttori europei di turbine ha visto i suoi margini operativi erodersi
notevolmente negli ultimi dodici mesi.
Questo é successo nonostante abbiano aumentato i prezzi del 30% nello stesso periodo e, durante la prima metá del 2022,
i risultati operativi sono stati addirittura negativi.
Nel breve periodo, é probabile che i costi di produzione si moderino soprattutto grazie al rallentamento dell'economia globale.
Resta peró il fatto che un'accelerazione della transizione verso le rinnovabili potrebbe provocare un ulteriore aumento delle materie prime e i costi di produzione.
Infatti, fino ad ora gli investimenti in rinnovabili si sono mantenuti a livelli molto bassi rispetto agli scenari stimati dalle agenzie internazionali
per raggiungere zero emissioni nette.

La seconda, e piú ovvia, questione delle fonti rinnovabili é la variabilitá.
Questo problema é alleviato se si tiene conto della non-correlazione tra fotovoltaico ed eolico nel corso di una giornata.
Purtroppo, la distribuzione geografica non aiuta molto.
Infatti, le condizioni atmosferiche sono correlate all'interno di una tipica nazione europe.
Questo é valido anche se si considera nazioni confinanti.
Riporto sotto una tabella delle correlazioni stimate tra diverse nazioni Europee dove si possono notare diversi gruppi di nazioni con alte correlazioni:
i paesi nordici (FI, EE, LV, LT, SE, DK), i paesi del centro Europa (FR, BE, NL, DE) o i paesi dell'est.

![Correlazione Vento](images/wind_correlations.jpeg)

Le soluzioni a basse emissioni ci sono ma o hanno una capacitá limitata come l'idroelettrico o sono costose
(batterie, produzione di idrogeno/ammoniaca o sovradimensionamento delle linee di trasmissione).
In pratica, la variabilitá delle rinnovabili é bilanciata da una sempre maggiore quota di generazione a gas naturale e, da quest'anno, carbone.
Oltre alla variabilitá, la presenza di una quota significativa di rinnovabili é deleteria alla rete elettrica perché viene a mancare l'effetto
stabilizzante dell'inerzia dei grandi generatori sulla frequenza.
Per esempio, ho imparato di recente che la trasmissione elettrica tra il nord e il sud della Svezia opera attualmente al 30% della capacitä
nominale a dispetto di una differenza di prezzi che arriva fino a dieci volte.
Questo perché al sud sono state chiuse le centrali nucleari che stabilizzavano la rete.
Queste problematiche sono particolarmente significative perché sono amplificate dal livello di penetrazione delle fonti rinnovabili.
Penso sia probabile che un introduzione ulteriore di fonti rinnovabili oltre una certa quota possa avere un rendimento addirittura negativo.

In sintesi, sono molto scettico riguardo le fonti rinnovabili come uniche soluzioni ad una produzione elettrica a basse emissioni.
Questo peró non fermerá il fanatismo "ecologista" a provarci.
Dopo aver cercato di spiegare la situazione a chi vuol sentire, non resta altro da fare che approfittare della situazione.


## Nucleare

Secondo me, l'energia nucleare é semplicemente necessaria per una transizione ad una produzione elettrica a basse emissioni.
È vero che ci si mette una decina d'anni a costruire una centrale nucleare ma questo problema veniva citato dai movimenti ecologisti dieci anni fa.
Prima iniziamo meglio é.
Purtroppo questo vuol dire che il nucleare non potrá aiutare a soddisfare la domanda energetica marginale mondiale nel breve periodo.
Spero solo che i decommissionamenti di centrali nucleari si fermino cosí da non esacerbare il problema.


# Strategie di investimento

Un portfolio classico si divide tra azioni e obbligazioni, tipicamente con quote di allocazione di 60% e 40% rispettivamente.
La dominanza di questo tipo di portfolio é dovuta da due fatti.
Il primo é che quelli azionari ed obbligazionari sono i piú gradi e liquidi mercati esistenti (se ci si limita alle obbligazioni statali Americane).
Quindi un tale portfolio puó essere adottato sia da singoli investitori fino ai fondi di investimento nazionali o pensionistici.
La seconda ragione é dovuta alle peculiaritá dei decenni appena trascorsi.
In questo periodo le obbligazioni statali sono state generalmente correlate negativamente con le azioni e quindi hanno permesso agli investitori
di ribilanciare verso le azioni in periodi di crisi economica o finanziaria. 
Le obbligazioni hanno avuto quindi un ruolo stabilizzante in questo tipo di portfolio, per giunta con un ritorno stimato positivo.
Purtoppo, come si é visto quest'anno, la correlazione tra azioni e obbligazioni diventa positiva in periodi di alta inflazione.
Un'allocazione che protegga il portfolio da questa eventualitá é quindi di vitale importanza.

## Tre fondi, un portfolio

La quantitá di investimenti diversi non é indicativo per il livello di diversificazione di un portfolio.
Si puó ottenere un portfolio decentemente diversificato con soli tre fondi:

* Azioni: Vanguard FTSE Developed World UCITS ETF USD Distributing (Ticker: VGVE in euro, VDEV in dollari).
  Questo fondo offre la piú ampia esposizione all'azionariato mondiale senza esposizione ai rischi
  relativi ad investimenti in azioni Cinesi.
* Obbligazioni: Vanguard USD Treasury Bond UCITS ETF (Ticker: VUTY in euro, VDTY in dollari).
  Non c'é bisogno di allocare in altre obbligazioni statali che non siano Americane.
  La denominazione in dollari rende tali obbligazioni ancor piú negativamente correlate con le azioni
  in periodi di crisi. 
* Trend following: Per esempio, Man AHL Trend Alternative DN H EUR Acc (ISIN: LU0851818400).
  I fondi trend following cercano di identificare in maniera automatica
  mercati futures che mostrano una direzione precisa e investono in modo appropiate (long/short).
  Tendono a mostrare un rendimento alto durante periodi inflazionari.
  Infatti sono nati proprio come strategia per investire nei mercati delle materie prime che sono
  caratterizzati da alta ciclicitá.
  Inoltre, queste strategia hanno rendimenti che, nel lungo periodo, tendono ad essere
  non correlati con quelli delle azioni.
  Il rischio principale sono i periodi di alta volatilitá e/o senza direzione precisa.
  Un'altro grande rischio in pratica é la somma totale di capitale gestita da questo tipo di fondi.
  Se la strategia diventa sovraffollata, i ritorni possono deteriorarsi significativamente.

Probabilmente assegnerei quote di 40%, 30% e 30% ai tre fondi rispettivamente,
forse 40%/40%/20% o 50%/30%/20% se si vuole deviare meno da un portfolio classico.
Ribilanciare il portfolio é molto importante e consiglierei di farlo
circa a metá dell'ultimo mese di ogni trimestre.

## Investimenti sul tema energetico

La peculiare situazione nell'ambito energetico offre secondo me una opportunitá di investimento
che beneficia da un'eventuale persistenza dell'inflazione e, allo stesso tempo, ha un alto
potenziale di ritorno.
In sintesi, un'allocazione che investe da un lato nel settore dei combustibili fossili e
dall'altro nell'estrazione delle materie prime necessarie per la proliferazione delle
rinnovabili e in generale, l'elettrificazione dell'economia.
La scelta di investire nelle materie prime e non nella produzione di energia rinnovabile é
dettata dal mio scetticismo riguardo al potenziale di ritorno di queste fonti.
Per 
Questa combinazione diminuisce di molto i rischi inerenti all'andamento dei prezzi dei singoli mercati.
L'unico scenario negativo rispetto al mercato che io possa immaginare per questa strategia
é se simultaneamente gli investimenti nel settore dei combustibili fossili aumentassero di molto
mentre quelli nel settore delle rinnovabili restassero a livelli bassi.
Questo significherebbe peró un abbandono di fatto della transizione energetica.

**Da notare** che, delle aziende che consiglio qui sotto, ho personalmente investito in
Vår Energi, Imperial Oil, Altius Minerals e Tourmaline Oil che rappresentano
5.66%, 5%, 2.55% e 1.75% del mio portfolio al 25/11/2022.
Il mio obiettivo é di arrivare ad allocazioni di circa 5% sia per Altius e Glencore
durante il prossimo anno.

Per quanto riguarda il settore delle materie prime, penso sia opportuno investire in aziende
con un portfolio di attivitá diversificato ma con esposizioni limitate a materie prime non
collegate al settore energetico.
Due aziende che suggerisco sono [Glencore](#Glencore) e [Altius Minerals](#Altius-Minerals).
Il primo é un produttore ed esportatore, il secondo é un finanziatore specializzato nel settore.
Entrambi hanno esposizioni significative al rame, metallo critico per l'elettrificazione
dell'economia e di difficile sostituzione.
L'unica alternativa é l'alluminio, materiale il cui costo di produzione é dettato prevalentemente dal costo
dell'energia.
Tra le due aziende si ottiene un'esposizione ad una varietá di materiali utilizzati nelle
batterie elettriche di vario tipo: litio, cobalto e vanadio.
Penso manchi solo il manganese e l'antimonio per coprire i materiali critici per la maggior parte
delle piú promettenti tipologie di batteria.
Altius offre anche l'esposizione diretta alla produzione da fonti rinnovabili
tramite la sussidiaria Altius Renewables (ARR, Toronto).
Entrambe hanno esposizione al carbone limitata dal punto di vista del valore assetti netto (NAV) ma
che potrebbe essere significativa nel prossimi anni a fronte di una crisi energetica prolungata.
Infatti tutti i paesi le cui forniture di gas vengono redirette verso l'Europa non hanno altra scelta
che riaccendere centrali a carbone.
Il rischio piú grande per entrambe é una crisi prolungata del settore immobiliare Cinese.
Glencore é in aggiunta esposta al rischio, seppur molto remoto, di liquidazione siccome il comparto
commerciale utilizza pesantemente contratti futures.

Per l'allocazione nel settore dei combustibili fossili invece la scelta é piú difficile
perché il rischio principale é puramente politico, ovvero l'implementazione eventuale di tasse sui profitti "straordinari".
Quali governi le hanno implementate o hanno piani a riguardo? E per quanto tempo resteranno in vigore?
Questi dettagli possono modificare pesantemente il valore di una azienda del settore.
L'esempio dell'inghilterra é lampante.
Sotto Johnson é stata introdotta una tassa aggiuntiva del 25% vigente fino al 2025.
Sunak ha successivamente aumentato la quota al 35% e spostato la scadenza al 2028.
Un'alternativa é scegliere aziende che operano in giurisdizioni che hanno giá livelli di tassazione molto alti.
In questo caso, la probabilitá di ulteriori aumenti dovrebbe essere minore.
Per esempio, suggerisco [Vår Energi](#Vår-Energi) che é lo spin-off degli assetti Norvegesi di Eni ed é sottoposta giá ad un
livello di tassazione del 78%. È difficile pensare ad un ulteriore aumento.
Un difetto consiste nella tassazione dei dividendi che, combinando la tassa Norvegese con quella Italiana,
arriverebbe al 45% circa.
La stessa Inghilterra potrebbe offrire opportunitá ora che la tassazione sul settore ha raggiunto livelli simili
alla Norvegia e resterá vigente a lungo.
Per esempio [Shell](#Shell-Plc) é un'azienda da considerare dato che l'Inghilterra (per adesso) non applica tasse
sui dividendi all'azionariato straniero.
Nei mercati nord americani le migliori occasioni secondo me si possono trovare in Canada.
[Imperial Oil](#Imperial-Oil) e [Tourmaline Oil](#Tourmaline-Oil) sono due aziende che meritano considerazione.
La prima ha piú di vent'anni di riserve senza il bisogno di investimenti ulteriori.
È quindi un'ottimo veicolo per proteggersi dall'eventualitá che la transizione dai combustibili fossili fallisca.
Possiede inoltre capacitá di raffinazione circa pari alla propria produzione quindi é protetta dalle dinamiche
dello sconto che spesso affligge il benchmark del petrolio Canadese, Western Canadian Select (WCS), rispetto
ad altre varietá di petrolio.
La seconda azienda opera prevalentemente nell'estrazione del gas naturale ed é, a mio parare, amministrata in maniera
lungimirante dal proprietario Mike Rose.
Il gas naturale Canadese, come il petrolio Canadese, é afflitto da una capacitá di esportazione molto deficitaria
e quindi rimane a livelli molto depressi rispetto al gas Americano o, ancor piú, a quello Europeo anche tenendo
in conto i costi di (re)gassificazione e transporto.
Secondo me il mondo avrá semplicemente bisogno del gas naturale Canadese ed un modo verrá trovato per esportarlo.
Il rischio in Canada é che la tassa sugli extra profitti non é stata ancora introdotta e c'é pressione per adottarla.

In definitiva, se dovessi scegliere solo due aziende nel settore dei combustibili fossili in base alle caratteristiche
puramente economiche sarei propenso verso Vår e Imperial.
Assieme a Glencore e Altius, queste quattro azioni potrebbero costituire un pacchetto per investire in questo tema.
La quota di questa strategia all'interno del portfolio potrebbe partire da un 10% divisa in parti uguali.

È sempre utile, ancor prima di effettuare un investimento, pensare alle condizioni di uscita.
Altius é un'azienda che si puó tranquillamente tenere attraverso tutto il ciclo a patto che
l'amministrazione mantenga la disciplina di investimento che hanno dimostrato nel passato.
Detto questo, un continuo ed eccessivo investimento nella sussidiaria ARR mi porterebbe
a vendere o ridurre la posizione.
Per quanto riguarda le altre tre azioni, é auspicabile uscire se il livello di investimenti nei 
rispettivi settori aumentasse significativamente.
Un tale scenario é riconoscibile dalle alte valutazioni assolute o relative al mercato,
significativa attivitá di fusioni/acquisizioni/quotazioni ed infine
alte quote di mercato del settore rispetto alla media storica.
Inoltre, per le aziende nel settore dei combustibili fossili considererei un uscita nel seguente scenario:
prezzi dei combustibili fossili depressi nonostante una crescita economica robusta e una produzione stagnante o in diminuzione. 

Se c'é l'intenzione, resta da discutere le tempistiche di investimento, questione sempre molto difficile,
e scegliere quali investimenti che potrebbero fare posto.

## Investimenti da evitare

Tutto secondo il mio modesto parere.
* Cina: il capitale é ostaggio del partito comunista, mancanza di revisioni esterne sui bilanci, rischio di conflitto
militare (tutti gli occhi su Taiwan ma a me pare che l'India possa essere un rischio sottovalutato).
Detto questo, non é il caso di disinvestire adesso siccome l'economia Cinese probabilmente riaprirá dai lockdown
il prossimo anno e il sentimento é molto negativo.
* Obbligazioni statali Europee: repressione finanziaria (rate reali negative) molto probabile che resti a lungo in Europa, 
fino a quando dura la crisi energetica l'Euro é destinato ad essere debole.


# Azioni

Descrizioni molto brevi per adesso...

## Altius Minerals

(Ticker: ALS, Toronto)

Azienda Canadese che investe in attivitá estrattive ottenendo in cambio royalties sulla produzione.
Possiede royalties su rame, ferro, potassio e altri metalli.
Oltre a investire in progetti esterni effettua un'attivitá di esplorazione con l'obbiettivo
di vendere i progetti ed ottenere royalties e una partecipazione azionaria.
Il 20% circa della capitalizzazione é attribuibile ad Altius Renewables, azienda controllata che opera in maniera simile,
cioé investimenti in cambio di royalties, ma nel settore della produzione elettrica da fonti rinnovabili.
Possiede inoltre investimenti significativi in Lithium Royalty Corp, azienda royalty non quotata specializza
nei progetti di litio.


## Ecora Resources

(Ticker: ECOR, London)

## Glencore

(Ticker: GLEN, London)

Azienda Svizzera che produce e commercia materie prime.
Gestisce miniere a bassi costi di produzione di rame, zinco e nickel in ordine di contributo alle entrate.
Inoltre ha progetti in sviluppo per l'estrazione di cobalto e vanadio, metalli critici per alcune tipologie
di batterie.
È attiva anche nel settore energetico con la produzione di carbone e la raffinazione del petrolio.
Queste ultime attivitá sono attualmente in declino gestito e non ricevono nuovi investimenti.
La peculiaritá di Glencore rispetto ad altre multinazionali quotate del settore risiede nel comparto commerciale.

## Imperial Oil

(Ticker: IMO, Toronto)

Azienda Canadese controllata da Exxon che produce e raffina petrolio.
Ha adottato la strategia di non investire in nuova produzione per concentrarsi ed ottimizzare i tre assetti
che sono in produzione attualmente.
Il piú importante é Kearl, un deposito di sabbia bituminosa che, per sua natura, non é soggetto al declino
produttivo caratteristico di depositi convenzionali o sottomarini.
Nonostante la cattiva reputazione di tale tipo di depositi, il costo e l'intensitá misurata in anidride-carbonica
é in costante diminuzione grazie a continui investimenti di ottimizzazione.
Oltre agli assetti produttivi, Imperial gestisce impianti di raffinazione di capacitá circa equivalente alla produzione.
Questo protegge l'azienda nei periodi di sovra-offerta del grezzo e la rende meno rischiosa rispetto a produttori non integrati.
Il rischio é ulteriormente diminuito dato il fatto che a fine 2022, Imperial con ogni probabilitá avrá debiti netti negativi.
L'amministrazione ha mostrato negli anni propensione a ritornare il capitale agli azionisti tramite i dividendi
bassi ma sempre crescenti e, soprattutto, con il riacquisto e conseguente ritiro di azioni.
Dal 2017 fino al 30/09/2022, la quantitá di azioni esistenti é diminuita del 25% ed é attualmente in corso una procedura
per acquistare circa il 3% di quelle rimanenti.

## Vår Energi

(Ticker: VAR, Oslo)

Spin-off degli assetti Norvegesi di ENI, quindi soggetta all'alta tassazione Norvegese sui combustibili fossili pari al 78%.
La produzione é suddivisa in circa 70% di petrolio e 30% di gas naturale misurata in contenuto energetico.
Per quanto riguarda le entrate, nel 2022 il contributo é stato 50%/50% dato gli alti prezzi del gas naturale.
Vår, nonostante la corta esistenza come azienda indipendente, ha avuto successo nell'esplorazione e sta investendo
in progetti che porteranno la produzione ad aumentare fino al 50% da oggi al 2025.
I rischi principali che non siano legati al prezzo dei combustibili fossili sono l'introduzione di un tetto ai prezzi del gas naturale per il mercato europeo oppure attentati alla rete di distribuzione. 

## Tourmaline Oil

(Ticker: TOU, Toronto)

## Eni Spa

(Ticker: ENI, Milano)

## Shell Plc

(Ticker: SHELL, Amsterdam)

